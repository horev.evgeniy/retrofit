package com.example.myretrofit

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myretrofit.api.ApiHelper
import com.example.myretrofit.api.RetrofitBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val apiHelper = ApiHelper(RetrofitBuilder.apiService)
    private val repository: Repository = Repository(apiHelper)

    private val _users = MutableLiveData<List<User>?>(null)
    val users: LiveData<List<User>?> = _users

    init {
        viewModelScope.launch {
            val result = repository.getUsers()
            _users.postValue(result)
        }
    }
}