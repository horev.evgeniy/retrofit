package com.example.myretrofit.api

import com.example.myretrofit.User
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET ("users")
    suspend fun getUsers(): List<User>

    @GET("users/{userId}")
    suspend fun getUser(@Path("userId") userId:String): User

}