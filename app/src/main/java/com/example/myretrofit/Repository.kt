package com.example.myretrofit

import com.example.myretrofit.api.ApiHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class Repository(private val apiHelper: ApiHelper) {
    suspend fun getUsers(): List<User> = withContext(Dispatchers.IO) {
        apiHelper.getUsers()
    }

}