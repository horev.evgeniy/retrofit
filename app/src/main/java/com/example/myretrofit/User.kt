package com.example.myretrofit

import com.google.gson.annotations.SerializedName

data class User(

    @SerializedName("Avatar")
    val avatar: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String

)
